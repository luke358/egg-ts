import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1588835906067_952';

  // add your egg config in here
  config.middleware = [];

  config.security = {
    csrf: {
      ignore: ['/api'],
    },
  };

  config.mongoose = {
    url: 'mongodb://127.0.0.1:27017/blog-egg',
  };

  config.cluster = {
    listen: {
      port: 3000,
      hostname: '127.0.0.1', // It is not recommended to set the hostname to '0.0.0.0', which will allow connections from external networks and sources, please use it if you know the risk.
      // path: '/var/run/egg.sock',
    },
  };

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
  };

  // the return config will combines to EggAppConfig
  return {
    ...config,
    ...bizConfig,
  };
};
