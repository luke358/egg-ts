import { Application } from 'egg';

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const blogSchema = new Schema(
    {
      title: { type: String, required: true, index: { unique: false } },
      info: { type: String, required: true },
      date: { type: Date, default: Date.now() },
      detail: {
        type: String,
        required: true,
        index: { unique: false },
      },
      // tag: { type: String },
      img: { type: String, default: null },
      // tags: [{ type: mongoose.SchemaTypes.ObjectId, ref: "Tag" }],
    },
    {
      timestamps: true,
    },
  );
  return mongoose.model('Blog', blogSchema);
};
