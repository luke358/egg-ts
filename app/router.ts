import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  router.get('/', controller.home.index);

  router.get('/h', controller.home.demo);

  router.resources('blogs', '/api/v1/blogs', controller.api.v1.blogs);
};
