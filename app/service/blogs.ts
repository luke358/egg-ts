import BaseService from './base';
import { Context } from 'egg';

export default class BlogsService extends BaseService {
  constructor(app: Context) {
    super('Blog', app);
  }
}

// import { Service } from 'egg';

// export default class BlogsService extends Service {
//   /**
//    * 分页获取数据
//    * @param query 查询条件
//    * @param page 当前页码
//    * @param per 每页显示数量
//    */
//   async list(query: any, page = 1, per = 10) {
//     const data = await this.app.model.Blog.find(query)
//       .limit(per)
//       .skip((page - 1) * per)
//       .sort({ id: -1 });
//     const totalCount = await this.app.model.Blog.count(query);
//     return {
//       totalCount,
//       pages: Math.ceil(totalCount / per),
//       data,
//     };
//   }
//   /**
//    * 根据Id查找
//    * @param id
//    */
//   async one(id: string) {
//     const data = await this.app.model.Blog.findById(id);
//     return data;
//   }

//   /**
//    * 根据Id修改
//    * @param id
//    * @param data 修改的数据
//    */
//   async update(id: string, data: any) {
//     const result = await this.app.model.Blog.findByIdAndUpdate(id, data, {
//       upsert: true, //查找更新，找不到则创建
//     });
//     return result;
//   }
//   /**
//    * 保存单条记录
//    * @param data 保存的数据
//    */
//   async save(data: any) {
//     const result = new this.app.model.Blog(data);
//     await result.save();
//     return result;
//   }
//   /**
//    * 保存多条数据
//    * @param models 保存数据的数组
//    */
//   async saveMany(models: any[]) {
//     const result = await this.app.model.Blog.insertMany(models);
//     return result;
//   }
//   /**
//    * 根据id删除单条
//    * @param id
//    */
//   async deleteOne(id: string) {
//     const result = await this.app.model.Blog.findByIdAndDelete(id);
//     return result;
//   }
//   /**
//    * 根据id删除多条数据
//    * @param ids g
//    */
//   async deleteMany(ids: string[]) {
//     const result = await this.app.model.Blog.remove({
//       $in: ids,
//     });
//     return result;
//   }
// }
