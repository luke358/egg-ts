import { Service, Context } from 'egg';

export default class BaseService extends Service {
  private model: string;

  /**
   * 初始化
   * @param model 模型名称
   * @param app 上下文
   */
  constructor(model: string, app: Context) {
    super(app);
    this.model = model;
  }

  /**
   * 分页获取数据
   * @param query 查询条件
   * @param page 当前页码
   * @param per 每页显示数量
   */
  async list(query = {}, page = 1, per = 10) {
    const data = await this.app.model[this.model]
      .find(query)
      .limit(per)
      .skip((page - 1) * per)
      .sort({ id: -1 });
    const totalCount = await this.app.model[this.model].count(query);
    return {
      currentPage: page,
      hasMore: page === Math.ceil(totalCount / per) ? false : true,
      totalCount,
      pages: Math.ceil(totalCount / per),
      data,
    };
  }
  /**
   * 根据Id查找
   * @param id
   */
  async one(id: string) {
    const data = await this.app.model[this.model].findById(id);
    return data;
  }

  /**
   * 根据Id修改
   * @param id
   * @param data 修改的数据
   */
  async update(id: string, data: any) {
    const result = await this.app.model[this.model].findByIdAndUpdate(
      id,
      data,
      {
        upsert: true, //查找更新，找不到则创建
      },
    );
    return result;
  }
  /**
   * 保存单条记录
   * @param data 保存的数据
   */
  async save(data: any) {
    const result = new this.app.model[this.model](data);
    await result.save();
    return result;
  }
  /**
   * 保存多条数据
   * @param models 保存数据的数组
   */
  async saveMany(models: any[]) {
    const result = await this.app.model[this.model].insertMany(models);
    return result;
  }
  /**
   * 根据id删除单条
   * @param id
   */
  async deleteOne(id: string) {
    const result = await this.app.model[this.model].findByIdAndDelete(id);
    return result;
  }
  /**
   * 根据id删除多条数据
   * @param ids g
   */
  async deleteMany(ids: string[]) {
    const result = await this.app.model[this.model].remove({
      $in: ids,
    });
    return result;
  }
}
