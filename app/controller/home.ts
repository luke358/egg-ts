import { Controller } from 'egg';

export default class HomeController extends Controller {
  public async index() {
    const { ctx } = this;
    ctx.body = await ctx.service.test.sayHi('zxc');
  }

  async demo() {
    const { ctx } = this;
    ctx.body = await ctx.service.blogs.list({});
  }
}
