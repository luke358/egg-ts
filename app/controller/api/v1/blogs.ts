import BaseController from './base';
import { Context } from 'egg';

export default class BlogsController extends BaseController {
  // private
  constructor(app: Context) {
    super('blogs', app);
  }
}

// import { Controller } from 'egg';

// export default class BlogsController extends Controller {
//   /**
//    * 列表
//    * get
//    */
//   async index() {
//     const { ctx } = this;
//     const result = await ctx.service.blogs.list({});
//     ctx.body = result;
//   }
//   /**
//    * 单个
//    * get
//    * /:id
//    */
//   async show() {
//     const { ctx } = this;
//     const result = await ctx.service.blogs.one(ctx.params.id);
//     ctx.body = {
//       code: 1,
//       msg: '获取单条记录',
//       data: result,
//     };
//   }

//   /**
//    * 新增
//    * post
//    */
//   async create() {
//     const { ctx } = this;
//     const result = ctx.service.blogs.save(ctx.request.body);
//     ctx.body = {
//       code: 1,
//       msg: '新增成功',
//       data: result,
//     };
//   }
//   /**
//    * 更新
//    * put
//    * /:id
//    */
//   async update() {
//     const { ctx } = this;
//     const result = await ctx.service.blogs.update(
//       ctx.params.id,
//       ctx.request.body,
//     );
//     ctx.body = {
//       code: 1,
//       msg: '修改成功',
//       data: result,
//     };
//   }

//   /**
//    * 单个删除
//    * delete
//    * /:id
//    */
//   async destroy() {
//     const { ctx } = this;
//     const result = await ctx.service.blogs.deleteOne(ctx.params.id);
//     ctx.body = {
//       code: 1,
//       msg: '删除成功',
//       data: result,
//     };
//   }

//   // async destory() {
//   //   const { ctx } = this;
//   //   ctx.body = {
//   //     code: 1,
//   //     msg: '删除成功',
//   //   };
//   // }
// }
