// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportHome from '../../../app/controller/home';
import ExportApiV1Base from '../../../app/controller/api/v1/base';
import ExportApiV1Blogs from '../../../app/controller/api/v1/blogs';

declare module 'egg' {
  interface IController {
    home: ExportHome;
    api: {
      v1: {
        base: ExportApiV1Base;
        blogs: ExportApiV1Blogs;
      }
    }
  }
}
